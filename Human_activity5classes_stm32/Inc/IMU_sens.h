#include "stm32l4xx_hal.h"
int32_t I2c_write(void *handle, uint8_t reg, uint8_t *bufp,
                              uint16_t len);
int32_t I2c_read(void *handle, uint8_t reg, uint8_t *bufp,
                             uint16_t len);
uint8_t  read_magnetometr( float *magnetic_field);
uint8_t read_imu(  float *angular_rate,float *acceleration);
uint8_t read_acceleration(float *acceleration);
uint8_t lsm9ds1_init(void);
