################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/IMU_senc.c \
../Src/app_x-cube-ai.c \
../Src/lsm9ds1_reg.c \
../Src/main.c \
../Src/network.c \
../Src/network_data.c \
../Src/stm32l4xx_hal_msp.c \
../Src/stm32l4xx_it.c \
../Src/syscalls.c \
../Src/system_stm32l4xx.c 

OBJS += \
./Src/IMU_senc.o \
./Src/app_x-cube-ai.o \
./Src/lsm9ds1_reg.o \
./Src/main.o \
./Src/network.o \
./Src/network_data.o \
./Src/stm32l4xx_hal_msp.o \
./Src/stm32l4xx_it.o \
./Src/syscalls.o \
./Src/system_stm32l4xx.o 

C_DEPS += \
./Src/IMU_senc.d \
./Src/app_x-cube-ai.d \
./Src/lsm9ds1_reg.d \
./Src/main.d \
./Src/network.d \
./Src/network_data.d \
./Src/stm32l4xx_hal_msp.d \
./Src/stm32l4xx_it.d \
./Src/syscalls.d \
./Src/system_stm32l4xx.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DUSE_HAL_DRIVER -DSTM32L496xx -I"C:/AI_models/Human_activity5classes/Human_activity5classes/Inc" -I"C:/AI_models/Human_activity5classes/Human_activity5classes/Middlewares/ST/AI/Inc" -I"C:/AI_models/Human_activity5classes/Human_activity5classes/Drivers/STM32L4xx_HAL_Driver/Inc" -I"C:/AI_models/Human_activity5classes/Human_activity5classes/Drivers/STM32L4xx_HAL_Driver/Inc/Legacy" -I"C:/AI_models/Human_activity5classes/Human_activity5classes/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"C:/AI_models/Human_activity5classes/Human_activity5classes/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


